#include <gtest/gtest.h>
#include "roman_converter.h"

TEST(RomanConverter, AllTestsInOneGo) {

    EXPECT_EQ( convert(1), "I" );
    EXPECT_EQ( convert(2), "II" );
    EXPECT_EQ( convert(3), "III" );
    EXPECT_EQ( convert(4), "IV" );
    EXPECT_EQ( convert(5), "V" );
    EXPECT_EQ( convert(6), "VI" );
    EXPECT_EQ( convert(7), "VII" );
    EXPECT_EQ( convert(8), "VIII" );
    EXPECT_EQ( convert(9), "IX" );
    EXPECT_EQ( convert(10), "X" );
    EXPECT_EQ( convert(11), "XI" );
    EXPECT_EQ( convert(12), "XII" );
    EXPECT_EQ( convert(13), "XIII" );
    EXPECT_EQ( convert(14), "XIV" );
    EXPECT_EQ( convert(15), "XV" );
    EXPECT_EQ( convert(16), "XVI" );

    EXPECT_EQ( convert(1732), "MDCCXXXII" );
    EXPECT_EQ( convert(3999), "MMMCMXCIX" );
    EXPECT_EQ( convert(3444), "MMMCDXLIV" );

};
